var express = require('express');
var router = express.Router();

/* GET Backend Index */
router.get('/', function(req, res) {
    res.json({
        "server status":'OK'
    });
});


module.exports = router;