FROM node:14-alpine

# Create app directory
WORKDIR /app
# Install app dependencies
COPY package.json .
RUN npm install

# Bundle app source
COPY . .

EXPOSE 9000
EXPOSE 5858

CMD ["sh", "-c", "npm run start"]