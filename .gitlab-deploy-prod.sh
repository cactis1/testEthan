set -f  

string=$PROD_DEPLOY_SERVER
ssh ubuntu@$PROD_DEPLOY_SERVER "cd team-1-api/ && sudo docker-compose down && cd .. && sudo rm -r team-1-api && git clone https://gitlab.com/cactis1/team-1-api.git && cd team-1-api/ && git checkout main && npm install  && sudo docker-compose up -d --build"